# ipa-downloader

A simple replacement for the `ironic-ipa-downloader` of Metal3. 
The container simply download the image from a URL defined in the
environment (`$IPA_URL`).

It usually points to the ironic image stored in the Nexus.

