#!/bin/sh

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


TAR=ironic-python-agent.tar
mkdir -p /shared/html/images
cd /shared/html/images || exit 1

echo "Retrieving IPA from ${IPA_URL}"

while true; do
    if curl --output "$TAR" "$IPA_URL"; then
        break
    fi
    echo "Error during file retrieving IPA."
    sleep 20
done

echo "Extracting IPA kernel and ramdisk"
tar -xf $TAR
if [ ! -f ironic-python-agent.kernel ]; then
    echo "Kernel not found"
    exit 1
fi
if [ ! -f ironic-python-agent.initramfs ]; then
    echo "Ramdisk not found"
    exit 1
fi
echo "Successful download"
rm $TAR
